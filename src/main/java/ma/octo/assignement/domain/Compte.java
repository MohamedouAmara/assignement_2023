package ma.octo.assignement.domain;

import javax.persistence.*;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Data;

import java.math.BigDecimal;

@Entity
@Table(name = "COMPTE")
@Data
public class Compte {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(length = 16, unique = true)
  private String nrCompte;

  private String rib;

  @Column(precision = 16, scale = 2)
  private BigDecimal solde;

  @ManyToOne()
  @OnDelete(action= OnDeleteAction.CASCADE )
  @JoinColumn(name = "utilisateur_id")
  private Utilisateur utilisateur;
}
