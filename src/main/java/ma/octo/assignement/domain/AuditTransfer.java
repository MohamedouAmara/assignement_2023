package ma.octo.assignement.domain;

import ma.octo.assignement.domain.util.EventType;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "AUDIT")
@Data
public class AuditTransfer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(length = 800)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;
}
