package ma.octo.assignement.domain;

import javax.persistence.*;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "TRAN")
@Data
public class Transfer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(precision = 16, scale = 2, nullable = false)
  private BigDecimal montantTransfer;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateExecution;

  @ManyToOne()
  @OnDelete(action= OnDeleteAction.CASCADE )
  private Compte compteEmetteur;

  @ManyToOne()
  @OnDelete(action= OnDeleteAction.CASCADE )
  private Compte compteBeneficiaire;

  @Column(length = 200)
  private String motifTransfer;
}
