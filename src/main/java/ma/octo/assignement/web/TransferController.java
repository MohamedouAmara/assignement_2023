package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.DepositService;
import ma.octo.assignement.service.ListerTransferService;
import ma.octo.assignement.service.TransferService;
import ma.octo.assignement.service.UtilisateurService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;


//@RestController(value = "/transfers")

@RestController(value = "/transfers")

class TransferController {


    Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    @Autowired
    private TransferService transferService;
    @Autowired
    private ListerTransferService listerTransferServices;
    @Autowired
    private UtilisateurService utilisateurService;
    @Autowired
    private CompteService compteService;
    @Autowired
    private DepositService depositService;
    

    @GetMapping("listOfTransferts")
    List<Transfer> loadAllTransfer() {
    	return listerTransferServices.loadAllTransfer(); 
    }

    @GetMapping("listOfAccounts")
    List<Compte> loadAllCompte() {

        return compteService.loadAllCompte();
    }

    @GetMapping("lister_utilisateurs")
       List<Utilisateur> loadAllUtilisateur(){
  		return utilisateurService.loadAllUtilisateur();
  	}


    @PostMapping("/executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public String createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
    	
    	transferService.createTransaction(transferDto);
    	return "Transfert a été realisé avec succées";
    	
    	
        
        
    }
    
    @PostMapping("/executerDeposite")
    @ResponseStatus(HttpStatus.CREATED)
    public String createTransaction(@RequestBody DepositDto depositDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
    	
    	depositService.createTransaction(depositDto);
    	
        
        return "Depot a été realisé avec succées";
    	
    }
}
