package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.Deposit;

public interface DepositRepository extends JpaRepository<Deposit, Long>{

}
