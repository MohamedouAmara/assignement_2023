package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.repository.TransferRepository;

@Service
@Transactional
public class ListerTransferService {
	
	
	Logger LOGGER = LoggerFactory.getLogger(ListerTransferService.class);
    @Autowired
    private TransferRepository transferRepository;
    
    public List<Transfer> loadAllTransfer() {
        LOGGER.info("Lister les transfer");
        List<Transfer> all = transferRepository.findAll();
        
            return CollectionUtils.isEmpty(all) ? null : all;
       
    }

}
