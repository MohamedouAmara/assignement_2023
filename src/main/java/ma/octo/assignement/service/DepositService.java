package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;

@Service
@Transactional
public class DepositService {
	
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private DepositRepository depositRepository;
    
    public static final int MONTANT_MAXIMAL = 10000;
    
    @Autowired
    private AuditService monService;
    
	 public String createTransaction(DepositDto depositDto)
	            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
	        Compte CompteBeneficiaire = compteRepository.findByNrCompte(depositDto.getNrCompteBeneficiaire());


	        if (CompteBeneficiaire == null) {
	            throw new CompteNonExistantException("Compte beneficiaire Non existant");
	        }

	        if (depositDto.getMontantDepose().equals(null) || depositDto.getMontantDepose().compareTo(BigDecimal.valueOf(0)) == 0) {
	            throw new TransactionException("Montant vide");
	        }
	        else if (depositDto.getMontantDepose().compareTo(BigDecimal.valueOf(10))== -1 ) {
	            throw new TransactionException("Montant minimal de transfer non atteint");
	        } 
	        else if (depositDto.getMontantDepose().compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL)) == 1) {
	            throw new TransactionException("Montant maximal de transfer dépassé");
	        }

	        if (depositDto.getMotifDeposit().length() == 0) {
	            throw new TransactionException("Motif vide");
	        }

	        
	        CompteBeneficiaire.setSolde(CompteBeneficiaire.getSolde().add(depositDto.getMontantDepose()));
	        compteRepository.save(CompteBeneficiaire);

	        Deposit deposit = new Deposit();
	        deposit.setDateExecution(new Date());
	        deposit.setCompteBeneficiaire(CompteBeneficiaire);
	        deposit.setNom_prenom_emetteur(depositDto.getNom_prenom_emetteur());
	        deposit.setMontant(depositDto.getMontantDepose());
	        deposit.setMotifDeposit(depositDto.getMotifDeposit());

	        depositRepository.save(deposit);

	        monService.auditDeposit("Transfer depuis " + depositDto.getNom_prenom_emetteur() + " vers " + depositDto
	                        .getNrCompteBeneficiaire() + " d'un montant de " + depositDto.getMontantDepose()
	                        .toString());
	        
	        return "Depot a été realisé avec succées";
	    }

}
