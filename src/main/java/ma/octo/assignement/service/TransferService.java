package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;

@Service
@Transactional
public class TransferService {

	
	Logger LOGGER = LoggerFactory.getLogger(TransferService.class);
    @Autowired
    private TransferRepository transferRepository;
    
    @Autowired
    private AuditService monService;
    
    @Autowired
    private CompteRepository compteRepository;
    
    public static final int MONTANT_MAXIMAL = 10000;
    
    public List<Transfer> loadAllTransfer() {
        LOGGER.info("Lister les transfer");
        List<Transfer> all = transferRepository.findAll();
        
            return CollectionUtils.isEmpty(all) ? null : all;
       
    }
	
    public String createTransaction(TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        Compte compteEmetteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte CompteBeneficiaire = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        if (compteEmetteur == null) {
            throw new CompteNonExistantException("Compte emetteur Non existant");
        }

        if (CompteBeneficiaire == null) {
            throw new CompteNonExistantException("Compte beneficiaire Non existant");
        }

        if (transferDto.getMontant().equals(null) || transferDto.getMontant().compareTo(BigDecimal.valueOf(0)) == 0) {
            throw new TransactionException("Montant vide");
        }
        else if (transferDto.getMontant().compareTo(BigDecimal.valueOf(10))== -1 ) {
            throw new TransactionException("Montant minimal de transfer non atteint");
        } 
        else if (transferDto.getMontant().compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL)) == 1) {
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (transferDto.getMotif().length() == 0) {
            throw new TransactionException("Motif vide");
        }

        // repeter deux fois
        
       /* if (compteEmetteur.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }*/

        if (compteEmetteur.getSolde().subtract(transferDto.getMontant()).compareTo(BigDecimal.valueOf(0)) == -1) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(transferDto.getMontant()));
        compteRepository.save(compteEmetteur);

        //eliminer new BigDecimal.
        
        CompteBeneficiaire.setSolde(CompteBeneficiaire.getSolde().add(transferDto.getMontant()));
        compteRepository.save(CompteBeneficiaire);

        Transfer transfer = new Transfer();
        transfer.setDateExecution(new Date());
        transfer.setCompteBeneficiaire(CompteBeneficiaire);
        transfer.setCompteEmetteur(compteEmetteur);
        transfer.setMontantTransfer(transferDto.getMontant());
        transfer.setMotifTransfer(transferDto.getMotif());

        transferRepository.save(transfer);

        monService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                        .toString());
        
        return "Transfert a été realisé avec succées";
        
    }

}
