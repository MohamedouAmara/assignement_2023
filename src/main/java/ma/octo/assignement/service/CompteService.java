package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;

@Service
@Transactional
public class CompteService {
	
    Logger LOGGER = LoggerFactory.getLogger(CompteService.class);

    @Autowired
    private CompteRepository compteRepository;
	
    public List<Compte> loadAllCompte() {
    	
    	LOGGER.info("Lister les comptes");
    	
        List<Compte> all = compteRepository.findAll();

        return CollectionUtils.isEmpty(all) ? null : all;
    }

}
