package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;

@Service
@Transactional
public class UtilisateurService {

	 @Autowired
	    private UtilisateurRepository utilisateurRepository;
	 
	 Logger LOGGER = LoggerFactory.getLogger(UtilisateurService.class);
	 
	    public List<Utilisateur> loadAllUtilisateur() {
	    	
	    	LOGGER.info("Lister les utilisateurs");
	    	
	        List<Utilisateur> all = utilisateurRepository.findAll();

	        return CollectionUtils.isEmpty(all) ? null : all;
	    }
}
