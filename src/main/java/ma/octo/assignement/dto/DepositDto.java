package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class DepositDto {
	
  private String nom_prenom_emetteur;
  private String nrCompteBeneficiaire;
  private String motifDeposit;
  private BigDecimal montantDepose;
  private Date date;
}
