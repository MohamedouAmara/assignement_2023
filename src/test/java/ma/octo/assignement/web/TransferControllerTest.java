package ma.octo.assignement.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;

@SpringBootTest
@AutoConfigureMockMvc
public class TransferControllerTest {

    @Autowired
    public MockMvc mockMvc;

    @Test
    public void testGetTransferts() throws Exception {
        mockMvc.perform(get("/listOfTransferts"))
            .andDo(print())
            .andExpect(status().isOk());
    }
    
    @Test
    public void testGetComptes() throws Exception {
        mockMvc.perform(get("/listOfAccounts"))
            .andDo(print())
            .andExpect(status().isOk());
    }
    
    @Test
    public void testGetUtilisateurs() throws Exception {
        mockMvc.perform(get("/lister_utilisateurs"))
            .andDo(print())
            .andExpect(status().isOk());
    }
    
    @Test
    public void testSetTransfer() throws Exception {
    	
    	//Contenu envoiyé dans la body de la request
    	
    	TransferDto transDto = new TransferDto();
    	
    	transDto.setNrCompteBeneficiaire("010000A000001000");
    	transDto.setNrCompteEmetteur("010000B025001000");
    	transDto.setMontant(BigDecimal.valueOf(10000));
    	transDto.setDate(new Date());
    	transDto.setMotif("Tester Insertion d'un transfert");
    	
    	//Rendre notre objet sous format jason pour l'envoyé dans le corps de la requette
    	
    	ObjectMapper json = new ObjectMapper();
    	String bodyFile = json.writeValueAsString(transDto);
    	
    	mockMvc.perform(post("/executerTransfers").contentType(MediaType.APPLICATION_JSON).content(bodyFile))
    	.andExpect(status().isCreated());
    	
    	
    }
    
    @Test
    public void testSetDepot() throws Exception {
    	
    	//Contenu envoiyé dans la body de la request
    	
    	DepositDto depotDto = new DepositDto();
    	
    	depotDto.setNom_prenom_emetteur("Amara Mohamedou");
    	depotDto.setNrCompteBeneficiaire("010000A000001000");
    	depotDto.setMontantDepose(BigDecimal.valueOf(10000));
    	depotDto.setDate(new Date());
    	depotDto.setMotifDeposit("Tester Depot d'un depot");
    	
    	//Rendre notre objet sous format jason pour l'envoyé dans le corps de la requette
    	
    	ObjectMapper json = new ObjectMapper();
    	String bodyFile = json.writeValueAsString(depotDto);
    	
    	mockMvc.perform(post("/executerDeposite").contentType(MediaType.APPLICATION_JSON).content(bodyFile))
    	.andExpect(status().isCreated());
    	
    	
    }
    
    
}
