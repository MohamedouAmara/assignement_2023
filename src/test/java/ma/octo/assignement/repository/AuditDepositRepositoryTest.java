package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import ma.octo.assignement.domain.AuditDeposit;
import ma.octo.assignement.domain.util.EventType;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;


@DataJpaTest
public class AuditDepositRepositoryTest {

  @Autowired
  private AuditDepositRepository auditDepositRepository;
	
  private AuditDeposit CreateAuditD(EventType eventType , String Message) {
	  
	  AuditDeposit auditDeposit = new AuditDeposit();
	  
	  auditDeposit.setEventType(eventType);
	  auditDeposit.setMessage(Message);
	  
	  return auditDeposit;
  }
  
  
  @Test
  public void findOneAuditDeposit() {
	  
	  AuditDeposit auditDeposit1 = CreateAuditD(EventType.DEPOSIT,"Un Depos a été effectuer");
	  
	  auditDepositRepository.save(auditDeposit1);
	  
	  AuditDeposit auditDeposit2 = CreateAuditD(EventType.DEPOSIT,"Un Depot2 a été effectuer");
	  
	  auditDepositRepository.save(auditDeposit2);
	  
	  
	  Optional<AuditDeposit> getAuditDepot = auditDepositRepository.findById(auditDeposit1.getId());

	  assertThat(getAuditDepot).isEqualTo(Optional.of(auditDeposit1));
  }

  @Test
  public void findAllAuditDeposit() {

	  AuditDeposit auditDeposit1 = CreateAuditD(EventType.DEPOSIT,"Un Depos a été effectuer");;
	  
	  auditDepositRepository.save(auditDeposit1);
	  
	  AuditDeposit auditDeposit2 = CreateAuditD(EventType.DEPOSIT,"Un Depot2 a été effectuer");
	  
	  auditDepositRepository.save(auditDeposit2);
	  
	  
	  Iterable<AuditDeposit> transfers = auditDepositRepository.findAll();
	  assertThat(transfers).hasSize(2).contains(auditDeposit1,auditDeposit2);
	
  }

  @Test
  public void saveDeposit() {
	  
	  AuditDeposit auditDeposit1 = CreateAuditD(EventType.DEPOSIT,"Un Depos a été effectuer");;
	  
	  auditDepositRepository.save(auditDeposit1);
	  
	  assertThat(auditDeposit1).hasFieldOrPropertyWithValue("eventType",EventType.DEPOSIT);
	  
  }

  @Test
  public void deleteAllDeposit() {
	  
	  		auditDepositRepository.deleteAll();

			assertThat(auditDepositRepository.findAll()).isEmpty();

  }
}