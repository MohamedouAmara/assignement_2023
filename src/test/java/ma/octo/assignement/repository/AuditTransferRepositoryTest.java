package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import ma.octo.assignement.domain.AuditTransfer;

import ma.octo.assignement.domain.util.EventType;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;


@DataJpaTest
public class AuditTransferRepositoryTest {

  @Autowired
  private AuditTransferRepository auditTransferRepository;
  
  private AuditTransfer CreateTrans(EventType eventType, String Message) {
	  
	  AuditTransfer auditTransfer = new AuditTransfer();
	  
	  auditTransfer.setEventType(eventType);
	  auditTransfer.setMessage(Message);
	  
	  return auditTransfer;
  }
	
  @Test
  public void findOneTransfer() {
	  
	  AuditTransfer auditTransfer1 = CreateTrans(EventType.TRANSFER,"Un Transfer a été effectuer");
	  
	  auditTransferRepository.save(auditTransfer1);
	  
	  AuditTransfer auditTransfer2 = CreateTrans(EventType.TRANSFER,"Un Transfer2 a été effectuer");
	  
	  auditTransferRepository.save(auditTransfer2);
	  
	  
	  Optional<AuditTransfer> getAuditTransfer = auditTransferRepository.findById(auditTransfer1.getId());

	  assertThat(getAuditTransfer).isEqualTo(Optional.of(auditTransfer1));
  }

  @Test
  public void findAllTransfer() {

	  AuditTransfer auditTransfer1 = CreateTrans(EventType.TRANSFER,"Un Transfer a été effectuer");
	  
	  auditTransferRepository.save(auditTransfer1);
	  
	  AuditTransfer auditTransfer2 = CreateTrans(EventType.TRANSFER,"Un Transfer2 a été effectuer");
	  
	  auditTransferRepository.save(auditTransfer2);
	  
	  
	  Iterable<AuditTransfer> auditTransfer = auditTransferRepository.findAll();
	  assertThat(auditTransfer).hasSize(2).contains(auditTransfer1,auditTransfer2);
  }

  @Test
  public void saveTransfer() {
	  
	  AuditTransfer auditTransfer = CreateTrans(EventType.TRANSFER,"Un Transfer a été effectuer");
	  
	  auditTransferRepository.save(auditTransfer);
	  
	  assertThat(auditTransfer).hasFieldOrPropertyWithValue("eventType",EventType.TRANSFER);
	  
  }

  @Test
  public void deleteAllTransfer() {
	  
	  	auditTransferRepository.deleteAll();

		assertThat(auditTransferRepository.findAll()).isEmpty();
  }
}