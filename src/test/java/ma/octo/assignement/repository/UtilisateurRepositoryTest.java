package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import ma.octo.assignement.domain.Utilisateur;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;


@DataJpaTest
public class UtilisateurRepositoryTest {

	  @Autowired
	  private UtilisateurRepository utilisateurRepository;
	
	  
	  private Utilisateur CreateUtilisateur(String userName, String lastName, String firstName,String gender) {
		  
			Utilisateur utilisateur = new Utilisateur();
			utilisateur.setUsername(userName);
			utilisateur.setLastname(lastName);
			utilisateur.setFirstname(firstName);
			utilisateur.setGender(gender);
		  
		  return utilisateur;
	  }
	  
	  
  @Test
  public void findOneUtilisateur() {
	  
		Utilisateur utilisateur1 = CreateUtilisateur("userAm", "last", "first","Male");;

		utilisateurRepository.save(utilisateur1);
	  
		Utilisateur utilisateur2 = CreateUtilisateur("userOs", "last1", "first1","Male");

		utilisateurRepository.save(utilisateur2);
	  
	  
	  Optional<Utilisateur> getUtilisateurs = utilisateurRepository.findById(utilisateur1.getId());

		assertThat(getUtilisateurs).isEqualTo(Optional.of(utilisateur1));
  }

  @Test
  public void findAllUtilisateur() {  
	  
	  List<Utilisateur> utilisateurs = utilisateurRepository.findAll();
	  assertThat(utilisateurs).hasSize(2);
  }

  @Test
  public void saveUtilisateur() {
	  
		Utilisateur utilisateur = CreateUtilisateur("userMe", "last2", "first2","Male");

		utilisateurRepository.save(utilisateur);
	  
	  assertThat(utilisateur).hasFieldOrPropertyWithValue("firstname","first2");
	  
  }

  @Test
  public void deleteAllUtilisateur() {
	  
	  	utilisateurRepository.deleteAll();

		assertThat(utilisateurRepository.findAll()).isEmpty();
  }
}