package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;


@DataJpaTest
public class TransferRepositoryTest {

  @Autowired
  private TransferRepository transferRepository;

  private Transfer CreateTransfer(Compte compteBeneficiaire,Compte compteEmetteur,Date date,Long montantTransfer,String motifTransfer) {
	  
	  Transfer transfer = new Transfer();
	  
	  transfer.setCompteBeneficiaire(compteBeneficiaire);
	  transfer.setCompteEmetteur(compteEmetteur);
	  transfer.setDateExecution(date);
	  transfer.setMontantTransfer(BigDecimal.valueOf(montantTransfer));
	  transfer.setMotifTransfer(motifTransfer);
	  
	  return transfer;
	  
  }	
  
  @Test
  public void findOneTransfer() {
	  
	  Transfer transfer1 = CreateTransfer(null,null,new Date(),140000L,"tester un transfer");
	  
	  transferRepository.save(transfer1);
	  
	  Transfer transfer2 = CreateTransfer(null,null,new Date(),180000L,"tester un transfer2");
	  
	  transferRepository.save(transfer2);
	  
	  
	  Optional<Transfer> getTransfer = transferRepository.findById(transfer1.getId());

		assertThat(getTransfer).isEqualTo(Optional.of(transfer1));
  }

  @Test
  public void findAllTransfer() {

	  Transfer transfer =  CreateTransfer(null,null,new Date(),140000L,"tester un transfer");
	  
	  transferRepository.save(transfer);
	  
	  Transfer transfer2 = CreateTransfer(null,null,new Date(),180000L,"tester un transfer2");
	  
	  transferRepository.save(transfer2);
	  
	  
	  Iterable<Transfer> transfers = transferRepository.findAll();
	  assertThat(transfers).hasSize(3).contains(transfer,transfer2);
  }

  @Test
  public void saveTransfer() {
	  
	  Transfer transfer = CreateTransfer(null,null,new Date(),140000L,"tester un transfer");
	  
	  transferRepository.save(transfer);
	  
	  assertThat(transfer).hasFieldOrPropertyWithValue("motifTransfer","tester un transfer");
	  
  }

  @Test
  public void deleteAllTransfer() {
	  
	  	transferRepository.deleteAll();

		assertThat(transferRepository.findAll()).isEmpty();
  }
}