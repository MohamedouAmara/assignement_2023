package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;


@DataJpaTest
public class DepositRepositoryTest {

  @Autowired
  private DepositRepository depositRepository;

	private Deposit CreateDeposit(Compte compteBenef,String compteEmett,Date date,Long Montant,String motifDepot) {
		
		  Deposit depot = new Deposit();
		  
		  depot.setCompteBeneficiaire(compteBenef);
		  depot.setNom_prenom_emetteur(compteEmett);
		  depot.setDateExecution(date);
		  depot.setMontant(BigDecimal.valueOf(Montant));
		  depot.setMotifDeposit(motifDepot);
		  
		  return depot; 
	}
	
	
  @Test
  public void findOneDeposit() {
	  
	  Deposit depot1 = CreateDeposit(null,"Emmeteur1",new Date(),140000L,"tester un depot1");
	  
	  depositRepository.save(depot1);
	  
	  Deposit depot2 = CreateDeposit(null,"Emmeteur2",new Date(),140000L,"tester un depot2");
	  
	  depositRepository.save(depot2);
	  
	  
	  	Optional<Deposit> getDepot = depositRepository.findById(depot1.getId());

		assertThat(getDepot).isEqualTo(Optional.of(depot1));
  }

  @Test
  public void findAllDeposit() {

	  Deposit depot1 = CreateDeposit(null,"Emmeteur1",new Date(),140000L,"tester un depot1");
	  
	  depositRepository.save(depot1);
	  
	  Deposit depot2 = CreateDeposit(null,"Emmeteur2",new Date(),160000L,"tester un depot2");
	  
	  depositRepository.save(depot2);
	  
	  
	  Iterable<Deposit> depots = depositRepository.findAll();
	  assertThat(depots).hasSize(2).contains(depot1,depot2);
  }

  @Test
  public void saveDeposit() {
	  
	  Deposit depot = CreateDeposit(null,"Emmeteur",new Date(),140000L,"tester un depot");
	  
	  depositRepository.save(depot);
	  
	  assertThat(depot).hasFieldOrPropertyWithValue("nom_prenom_emetteur","Emmeteur");
	  
  }

  @Test
  public void deleteAllDeposit() {
	  
	  	depositRepository.deleteAll();

		assertThat(depositRepository.findAll()).isEmpty();
  }
}