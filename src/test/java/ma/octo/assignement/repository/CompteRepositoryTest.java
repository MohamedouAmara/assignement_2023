package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Optional;


@DataJpaTest
public class CompteRepositoryTest {

  @Autowired
  private CompteRepository compteRepository;
  @Autowired
  private UtilisateurRepository utilisateurRepository;
  
  
  private Utilisateur CreateUtilisateur(String userName, String lastName, String firstName,String gender) {
	  
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setUsername(userName);
		utilisateur.setLastname(lastName);
		utilisateur.setFirstname(firstName);
		utilisateur.setGender(gender);
	  
	  return utilisateur;
  }
  

  private Compte CreateCompte(String nrCompte, String rib, Long solde,Utilisateur user) {
	  
	  Compte compte = new Compte();
	  
	  compte.setNrCompte(nrCompte);
	  compte.setRib(rib);
	  compte.setSolde(BigDecimal.valueOf(solde));
	  compte.setUtilisateur(user);
	  
	  return compte;
  }
  
  @Test
  public void findOneCompte() {
	  
		Utilisateur utilisateur1 = CreateUtilisateur("userAm", "last", "first","Male");

		utilisateurRepository.save(utilisateur1);
	  
		Utilisateur utilisateur2 = CreateUtilisateur("userOs", "last1", "first1","Male");

		utilisateurRepository.save(utilisateur2);
		
	  Compte compte1 = CreateCompte("010000A00000100A", "RIB1", 140000L ,utilisateur2);
	  
	  compteRepository.save(compte1);
	  
	  Compte compte2 = CreateCompte("010000B02500100Z", "RIB2", 170000L ,utilisateur1);
	  
	  compteRepository.save(compte2);
	  
	  
	  Optional<Compte> getCompte = compteRepository.findById(compte1.getId());

		assertThat(getCompte).isEqualTo(Optional.of(compte1));
  }

  @Test
  public void findAllCompte() {	  
	  
	  Iterable<Compte> compte = compteRepository.findAll();
	  assertThat(compte).hasSize(2);
  }

  @Test
  public void saveCompte() {
	  
		Utilisateur utilisateur = CreateUtilisateur("userMed", "last2", "first2","Male");

		utilisateurRepository.save(utilisateur);
 
	  Compte compte = CreateCompte("010000A000001009", "RIB", 140000L ,utilisateur);
	  
	  compteRepository.save(compte);
	  
	  assertThat(compte).hasFieldOrPropertyWithValue("nrCompte","010000A000001009");
	  
  }

  @Test
  public void deleteAllCompte() {
	  
	  	compteRepository.deleteAll();

		assertThat(compteRepository.findAll()).isEmpty();
  }
}